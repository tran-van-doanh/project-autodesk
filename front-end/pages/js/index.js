$(document).ready(function () {
  $("#hide-ctrl").click(function () {
    if ($(".isShow").css("display") === "block") {
      $("#hide-ctrl").html("<span class='material-icons'>east</span>");
      $(".isShow").css("display", "none");
      $(".col-2").css("width", "55px");
      $(".body-content").css("padding", "0 20px 0 75px");
      $(".icon-hidden").css("display", "none");
      $(".icon-show").css("display", "block");
    } else {
      $(".isShow").css("display", "block");
      $("#hide-ctrl").html(
        "<span class='material-icons'>keyboard_backspace</span>"
      );
      $(".col-2").css("width", "240px");
      $(".body-content").css("padding", "0 20px 0 260px");
      $(".icon-hidden").css("display", "block");
      $(".icon-show").css("display", "none");
    }
  });
  function handleCancelPublish() {
    window.location.href = "../sheets/index.html";
  }
  function handleSaveUpload() {
    window.location.href = "../sheets/index.html";
  }
  $("#parent").click(function () {
    $(".child").prop("checked", this.checked);
  });
  $(".nav-link").click(function () {
    $("#parent").prop("checked", false);
    $(".child").prop("checked", false);
  });
  $(".child").click(function () {
    if ($(".child:checked").length == $(".child").length) {
      $("#parent").prop("checked", true);
    } else {
      $("#parent").prop("checked", false);
    }
  });
  $(function () {
    $('[data-toggle="tooltip"]').tooltip({ delay: { show: 500 } });
  });
});
